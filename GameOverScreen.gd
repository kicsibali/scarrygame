extends ColorRect

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var endgamestarted = false
var endgametime = 10.0
var alpha = 0.0
var player

# Called when the node enters the scene tree for the first time.
func _ready():
	rect_size = get_viewport().size
	visible = false
	player = get_tree().root.find_node("Player", true, false)
	
func _physics_process(delta):
	if endgamestarted and endgametime > 0.0:
		endgametime -= delta
		
	if endgametime < 0.0:
		visible = true
		player.inp_ena = false
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		


func _on_Button_pressed():
	get_tree().quit()
