extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player : Spatial
var endgamescreen
var ppmat : ShaderMaterial
var speed_mods = [1.0, 2.0, 4.0, 8.0]
var damage = [0.04, 0.08, 0.16, 0.2]
var teleport_cc = [30.0, 20.0, 10.0, 5.0]
var curcc = 30.0
var stage = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_node("../Player")
	endgamescreen = get_tree().root.find_node("EndGame", true, false)
	var cont : ViewportContainer = get_node("../../..")
	ppmat = cont.material
	
	
func teleport():
	if curcc <= 0:
		var dir = (player.translation - translation)
		translation = player.translation - (dir.normalized() * 12.0)
		curcc = teleport_cc[stage]
	
func _physics_process(delta):
	if curcc > 0.0:
		curcc -= delta
	
	if stage < 4:
		var dir = (player.translation - translation)
		translate(dir.normalized() * speed_mods[stage] * delta)
		
		if dir.length() < 30.0:
			var modif = 1.0 - (dir.length() / 30.0)
			ppmat.set_shader_param("camod", modif* 0.04)
		else:
			ppmat.set_shader_param("camod", 0.0)
			
		if dir.length() < 10.0:
			player.damage(delta * damage[stage] * (1.0 - dir.length() / 10.0))
		if dir.length() > 15.0:
			teleport()
	else:
		visible = false
		ppmat.set_shader_param("camod", 0.0)
		endgamescreen.endgamestarted = true
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
