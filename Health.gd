extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var c1 = Color(0.0, 0.0, 0.0)
export var c2 = Color(0.0, 0.0, 0.0)
export var c3 = Color(0.0, 0.0, 0.0)
export var c4 = Color(0.0, 0.0, 0.0)

var grad

var player

# Called when the node enters the scene tree for the first time.
func _ready():
	$ColorRect.rect_position = Vector2(0.0, get_viewport().size.y-$ColorRect.rect_size.y)
	grad = Gradient.new()
	grad.set_color(0, c4)
	grad.set_color(1, c1)
	grad.add_point(0.25, c4)
	grad.add_point(0.50, c3)
	grad.add_point(0.75, c2)
	player = get_parent().get_node("ViewportContainer/Viewport/Spatial").find_node("Player")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	$ColorRect.color = grad.interpolate(player.health)
