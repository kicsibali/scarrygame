extends ColorRect

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var onfadein = false
var alpha = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	rect_size = get_viewport().size
	modulate = Color(1.0, 1.0, 1.0, alpha)
	visible = false
	
func _physics_process(delta):
	if onfadein and alpha < 1.0:
		visible = true
		alpha += delta / 2
		modulate = Color(1.0, 1.0, 1.0, alpha)


func _on_TextureButton_pressed():
	get_tree().reload_current_scene()
