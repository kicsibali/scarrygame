extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player : Spatial
var onheal = false
var healcap = 0.3

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_tree().get_root().find_node("Player", true, false)
	
func _physics_process(delta):
	if not onheal and (player.translation - translation).length() < 3.0 and player.health < 0.8:
		onheal = true
		visible = false
		
	if onheal and healcap > 0.0:
		healcap -= 0.1 *delta
		if player.health < 1.0:
			player.health += 0.1 *delta
		
	if healcap < 0.0:
		queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
