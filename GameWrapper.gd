extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var player

# Called when the node enters the scene tree for the first time.
func _ready():
	$ViewportContainer/Viewport.size = get_viewport().size
	player = find_node("Player")
	
func setWeight(w):
	var material : ShaderMaterial = $ViewportContainer.material
	material.set_shader_param("weight", w)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	setWeight(1.0 - player.health)
