extends KinematicBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var inp_ena = true

var rot = Vector2(0.0, 0.0)
var speed = 5.0
var G = -24.0

var ap = 8.0
var oob = 0.0

const MAX_SPEED = 10
const MAX_SPRINT = 24
const JUMP_SPEED = 12
const ACCEL = 3.0

const DEACCEL= 10.0
const MAX_SLOPE_ANGLE = 40

var velo = Vector3(0.0, 0.0, 0.0)

var flash
var flash_switch

var health = 1.0
var viewport_container : ViewportContainer

var book : Spatial

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	flash = $Camera/SpotLight
	flash_switch = false
	flash.light_energy = 0.0
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func damage(damage : float):
	health -= damage
	

func die():
	inp_ena = false
	get_tree().root.find_node("GameOver", true, false).onfadein = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func process_lookray():
	var space_state = get_world().direct_space_state
	var camera : Camera = $Camera
	var from = camera.project_ray_origin(get_viewport().size / 2.0)
	var to = from + camera.project_ray_normal(get_viewport().size / 2.0) * 5.0
	var result = space_state.intersect_ray(from, to, [self, $Camera], 2, true, true)
	if result:
		if result.get("collider").get_parent().has_method("onCollect"):
			book = result.get("collider").get_parent()
			$Label.visible = true
	else:
		$Label.visible = false
		book = null
		
	if Input.is_action_just_pressed("collect"):
		if book != null:
			book.onCollect()
			book = null

func process_movement(delta):
	rotation_degrees = Vector3(0.0, -rot.x, 0.0)
	$Camera.rotation_degrees = Vector3(-rot.y, 0.0, 0.0)
	
	if Input.is_action_just_pressed("flash"):
		if flash_switch:
			flash.light_energy = 0.0
		else:
			flash.light_energy = 2.0
		flash_switch = !flash_switch
	
	
	var movedir = Vector3(0.0, 0.0, 0.0)
	if Input.is_action_pressed("forward"):
		movedir += Vector3(0.0, 0.0, -1.0)
	if Input.is_action_pressed("backward"):
		movedir += Vector3(0.0, 0.0, 1.0)
	if Input.is_action_pressed("left"):
		movedir += Vector3(-1.0, 0.0, 0.0)
	if Input.is_action_pressed("right"):
		movedir += Vector3(1.0, 0.0, 0.0)
		
	movedir = movedir.normalized()
	
	var dir = Vector3(0.0, 0.0, 0.0)
	
	
	dir += transform.basis.x * movedir.x
	dir += transform.basis.z * movedir.z 
	
	if is_on_floor():
		if Input.is_action_just_pressed("jump"):
			velo.y = JUMP_SPEED
	
	var hvel = velo
	hvel.y = 0.0
	
	if oob > 0.0:
		oob -= delta
	
	var target = dir
	if Input.is_action_pressed("sprint"):
		if ap > 0.0 and oob <= 0.0:
			ap -= delta
			target *= MAX_SPRINT
			if ap <= 0.0:
				oob = 3.0
		if oob > 0.0:
			target *= MAX_SPEED
			if ap < 8.0:
				ap += delta * 0.8
	else:
		target *= MAX_SPEED
		if ap < 8.0:
			ap += delta * 0.8
	
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	
	hvel = hvel.linear_interpolate(target, accel * delta)
	velo.y += delta * G
	velo.x = hvel.x
	velo.z = hvel.z
	velo = move_and_slide(velo, Vector3(0.0, 1.0, 0.0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE), false)

func _physics_process(delta):
	if inp_ena:
		process_movement(delta)
	process_lookray()
	
	if health <= 0.0:
		die()

func _input(event):
	if inp_ena and event is InputEventMouseMotion:
		rot += event.relative
		rot.x = fmod(rot.x, 360.0)
		rot.y = clamp(rot.y, -90.0, 90.0)